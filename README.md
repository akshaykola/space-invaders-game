# Space Invaders Game
This is a clone of the original game Space Invaders. I used Python 3.6.4 and Turtle graphics to create it.

You can find some more detailed information about the original game [here](https://en.wikipedia.org/wiki/Space_Invaders). You are a lone defender defending your planet from hordes of space invaders. The invaders move left and right on the screen - when one invader reaches a border, all of the invaders drop down one row towards the planet.  You have a weapon that can only be fired once at a time.

# Screenshot
![screenshot](screenshots/screenshot.PNG)